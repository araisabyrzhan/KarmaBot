var BotHelper = require('./BotFunctions')
var logindex = 1;
var regindex = 2;
var questions = [
  {index: 0,
    qType: 'text',
    options: BotHelper.makeButtons(['Войти','Зарегистрироваться'],['log','reg'],1,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^log/i,
        next:logindex
      },
      {
        AnsType: 'inline',
        reg:/^reg/i,
        next:regindex
      }
  ],
    data: 'Добро пожаловать в приложение SIGA.\nПожалуйста, для начала пройдите регистрацию или войдите.'
  },
  {index: 1,
    qType: 'text',
    options: BotHelper.makeButtons(['Да','Нет'],['yes','no'],2,true),
    data: 'Это ведь ваш Аккаунт?:',
    err: 'Выберите, нажав кнопочку выше :)',
    answers:[
      {
        AnsType: 'inline',
        reg:/^yes/i,
        next: 2
      },
      {
        AnsType: 'inline',
        reg:/^no/i,
        next: 0
      }
    ]
  },
  {index: 2,
    qType: 'text',
    options: {},
    data: 'Введите ваш Email или Телефон:',
    err: 'Введите ваш Email или Телефон',
    answers:[
      {
        AnsType: 'text',
        reg:/([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,5})/i
      },
      {
        AnsType: 'text',
        reg:/([0-9])/i
      }
    ]
  },
  {index: 3,
    qType: 'text',
    options: {},
    data: 'Введите пароль:',
    err: 'Введите пароль от аккаунта который вы указали.',
    answers:[
      {
        AnsType: 'text',
      }
    ]
  }





]


module.exports = questions
