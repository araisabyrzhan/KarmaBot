var express = require('express');
var bodyParser = require('body-parser');
// var mongoose = require('mongoose');
var path =require('path');
const app = express()

var index = require('./routes/index')
// mongoose.connect('mongodb://localhost:27017/SigaProject')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api', index)

app.get('/style.css', (req, res) => {
res.sendFile('style.css', { root: path.join(__dirname, '../static') });
})

app.get('*', (req, res) => {
res.sendFile('index.html', { root: path.join(__dirname, '../static') });
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.send(500, { message: err.message })
})



// app.listen(8001, () => {
//   console.log(`is now up and running on 8000`)
// })
// Exporting server
// export default { app }
module.exports = app
