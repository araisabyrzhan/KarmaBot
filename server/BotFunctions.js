

/*
                                          Telegram Bot Helper
Usage:
0) var BotHelper = require('./BotFunctions');
  0.1 BotHelper.askFor(bot, questions, trigger, start_callback, mid_callback, end_callback, stop_callback) {

1) bot - экзэмпляр вашего бота.

2) questions - Вопросы.
  2.1 Формат вопроса - массив с объектами. Пример объекта:
  {
    index: 0,                                   - Индекс вопроса. Обязательный параметр. Должен начинаться с 0.
    qType: 'text',                              - Тип вопроса. Все типы будут описаны в пункте 2.2
    options: {},                                - Дополнительные опции к сообщению. Тут могут быть как кнопки так и caption для фотографий
    data: 'Какое-то сообщение',                 - Сообщение которое будет отправляться человеку. Если qType равен photo, тут должен быть путь к фотографии.
    err: 'Просто напишите любое слово',         - Сообщение об ошибке. Срабатывает если тип ответа не совпадает с ожидаемым. Если AnsType равен photo, а человек написал простое сообщение, то ему отправится данное сообщение.
    answers: [                                  - Массив ожидаемых ответов.
      {
        AnsType: 'inline',                      - Ожидаемый тип ответа. Если тип ответа - document, то вместо reg можно использвать mime. (application/pdf)
        reg: /привет (.+) урод/i,               - Регулярка. Не обязательный параметр. Если Регулярка указана, то пришедший ответ проверяется через неё.
        next: 4                                 - Индекс вопроса который задастся, если пришедший ответ удовлетворит условиям (регулярке/типу). Если не указан, задается следующий по индексу вопрос.
      },
      {
        AnsType: 'inline',
        reg: /Hello/i,
        next: 3
      },
      {
        AnsType: 'text',                         - Чем ниже условие тем оно приоритетнее.
        reg: /(.+)/i,
        next: 5
      }
    ]
  }
  2.2 Все доступные типы вопросов\ответов:
    text[msg.text], photo[msg.photo[0].file_id], video[msg.video.file_id], voice[msg.voice.file_id], audio[msg.audio.file_id], location[msg.location], document[msg.document], contact[msg.contact], sticker[msg.sticker], inline[msg.data](только приём)
3) Trigger - Включатель и выключатель. Триггером должен быть *Объект объявленный глобально*
  3.1 Вид объекта:
  var trig = {                                  - Обязательно объявить глобально.
    stopOn: {                                   - секция отвечающая за Остановку опроса.
      force: [],                                - Массив изначально пустой. В течении работы программы, если вы захотите принудительно завершить опрос для определенного пользователя вы пушите в него данные.
                                                - {stop:true, uid:<ID пользователя>} при этом опрос завершается принудительно.
      data: '/stop',                            - Слово \ данные при получении которых срабатывает остановка. Тип остановки не принудительный.
      AnsType: 'text',                          - Тип данных
      reg: /a(.+)d/i                            - Не обязательно. Регулярка при совпадении с которой, завершается опрос.
    },
    startOn: {                                  - Секция отвечающая за Начало опроса.
      data: '/start',                           ~ Тоже самое что и при остановке
      AnsType: 'text',                          ~
      reg: /\/start/i,                          ~
    }
  }

4) start_callback - Колбэк срабатывающий при старте. если ваше стартовое слово /start и кто-то его напишет то он срабатывает.
  4.1 Параметры:
    1 - ID человека,
    2 - Firstname Человека,
    3 - Объект message,
    4 - Callback - ***** Обязательный ***** Вызывается С двумя параметрами: OK и Next
      4.1 OK -Если True, Говорит о том, что можно продолжать опрашивать. Если False то останавливает опрос.
      4.2 Next - Не обязательный параметр - Говорит какой вопрос должен идти следующим.
5) mid_callback Срабатывающий при каждом валидном ответе Человека
  5.1 Параметры:
    1 - ID человека,
    2 - Массив всех ответов человека,
    3 - Последний ответ человека,
    4 - Callback - ***** Обязательный ***** Вызывается С двумя параметрами: OK и Next
      4.1 OK -Если True, Говорит о том, что можно продолжать опрашивать. Если False то останавливает опрос.
      4.2 Next - Не обязательный параметр - Говорит какой вопрос должен идти следующим.
6) end_callback Срабатывает при ответе на последний вопрос.
  6.1 Параметры:
    1 - ID человека,
    2 - Массив всех ответов человека,
    3 - Последний ответ человека.
7) stop_callback Колбэк срабатывающий при остановке опроса.
  7.1 Параметры:
    1 - ID человека,
    2 - Boolean: Если True то остановлен принудительно (через push), если False - Остановлен через колбэк,
    3 - Массив всех ответов человека.

Made by >Дамир Инкарпарейтед<                         Никакие права не защищены ©
*/
function askFor(bot, questions, trigger, start_callback, mid_callback, end_callback, stop_callback) {
  var sessions = []

  function delById(id) {
    var f = false;
    var t = -1;
    sessions.forEach(function(s, i) {
      if (s.id == id) {
        t = i;
        f = true;
      }
    })
    if (!f) {
      return false;
    } else {
      sessions.splice(t, 1);
    }
  }
  function getAnswers(id) {
    var ans = []
    sessions.forEach(function(s) {
      if (s.id == id) {
        ans = s.answers;
      }
    })
    return ans;
  }
  function getStage(id) {
    var f = false;
    var t = -1;
    sessions.forEach(function(s) {
      if (s.id == id) {
        f = true;
        t = s.stage
      }
    })
    return t;
  }
  function getByIndex(index) {
    var r = false;
    questions.forEach(function(q) {
      if (q.index == index) {
        r = q;
      }
    })
    return r;
  }
  function prevStage(id) {

    sessions.forEach(function(s) {
      if (s.id == id) {
        if (stage) {
          s.stage = stage;
          s.nav = newNav(s.nav,s.stage)
        }
      }
    })
  }
  function nextStage(id, stage) {
    function newNav(id,oldNav,st) {
      var n = oldNav;
      if(oldNav.indexOf(st)<0) {
        n.push(st)
      }
      return n;
    }
    var f = false;
    sessions.forEach(function(s) {
      if (s.id == id) {
        f = true;
        if (stage) {
          s.stage = stage;
          s.nav = newNav(id, s.nav,s.stage)
        } else {
          s.stage++
          s.nav = newNav(id, s.nav,s.stage)
        }
      }
    })
    if (!f) {
      sessions.push({
        id: id,
        stage: 0,
        nav: [0],
        answers: [],
        lastChat: 0
      });
    }
    return true;
  }
  function sendByType(id, comp, index) {
    var op = getByIndex(index);
    switch (op.qType.toLowerCase()) {
      case 'text':
        bot.sendMessage(id, op.data, op.options);
        break;
      case 'photo':
        bot.sendPhoto(id, op.data, op.options);
        break;
      case 'video':
        bot.sendVideo(id, op.data, op.options);
        break;
      case 'voice':
        bot.sendVoice(id, op.data, op.options);
        break;
      case 'audio':
        bot.sendAudio(id, op.data, op.options);
        break;
      case 'location':
        bot.sendLocation(id, op.data, op.options);
        break;
      case 'contact':
        bot.sendContact(id, op.data, op.options);
        break;
      case 'document':
        bot.sendDocument(id, op.data, op.options);
        break;
      case 'sticker':
        bot.sendSticker(id, op.data, op.options);
        break;
      default:
        return false;
    }
  }
  function compareByData(message, comp) {
    switch (comp.AnsType.toLowerCase()) {
      case 'text':
        if (message.text) {
          if (comp.reg) {
            if (comp.reg.test(message.text)) {
              return true
            } else {
              return false
            }
          } else {
            if (comp.data.toLowerCase() != message.text.toLowerCase()) {
              return false;
            } else {
              return true;
            }
          }
        }
        break;
      default:
        return true;
    }
  }
  function compareByType(message, comparator) {
    if (comparator.answers) {
      var fnd = false;
      comparator.answers.forEach(function(comp, i) {
        switch (comp.AnsType.toLowerCase()) {
          case 'text':
            if (message.text) {
              if (comp.reg) {
                // if (message.text.search(comp.reg) != -1) {
                if (comp.reg.test(message.text)) {
                  fnd = {
                    res: message.text,
                    index: comparator.index,
                    next: comp.next
                  }
                }
              } else {
                fnd = {
                  res: message.text,
                  index: comparator.index,
                  next: comp.next
                }
              }
            }
            break;
          case 'photo':
            if (message.photo) {
              fnd = {
                res: message.photo[message.photo.length-1].file_id,
                index: comparator.index,
                next: comp.next
              }
            }
            break;
          case 'video':
            if (message.video) {
              fnd = {
                res: message.video.file_id,
                index: comparator.index,
                next: comp.next
              }
            }
            break;
          case 'voice':
            if (message.voice) {
              fnd = {
                res: message.voice.file_id,
                index: comparator.index,
                next: comp.next
              }
            }
          case 'audio':
            if (message.audio) {
              fnd = {
                res: message.audio.file_id,
                index: comparator.index,
                next: comp.next
              }
            }
            break;
          case 'location':
            if (message.location) {
              fnd = {
                res: message.location,
                index: comparator.index,
                next: comp.next
              }
            }
            break;
          case 'contact':
            if (message.contact) {
              fnd = {
                res: message.contact,
                index: comparator.index,
                next: comp.next
              }
            }
            break;
          case 'document':
            if (message.document) {
              if (comp.mime) {
                if (comp.mime.toLowerCase() == message.document.mime_type.toLowerCase())
                  fnd = {
                    res: message.document,
                    index: comparator.index,
                    next: comp.next
                }
              } else {
                fnd = {
                  res: message.document,
                  index: comparator.index,
                  next: comp.next
                }
              }
            }
            break;
          case 'sticker':
            if (message.sticker) {
              fnd = {
                res: message.sticker,
                index: comparator.index,
                next: comp.next
              }
            }
          case 'inline':
            if (message.data) {
              if (comp.reg) {
                if (comp.reg.test(message.data)) {
                  fnd = {
                    res: message.data,
                    index: comparator.index,
                    next: comp.next
                  }
                }
              } else {
                fnd = {
                  res: message.data,
                  index: comparator.index,
                  next: comp.next
                }
              }
            }

            break;
          default:
            fnd = false;
        }
      })
      return fnd;
    } else {
      var comp = comparator;
      switch (comp.AnsType.toLowerCase()) {
        case 'text':
          if (message.text) {
            if (comp.reg) {
              // if (message.text.search(comp.reg) != -1) {
              if (comp.reg.test(message.text)) {
                return {
                  res: message.text,
                  index: comparator.index,
                  next: comp.next
                }
              }
            } else {
              return {
                res: message.text,
                index: comparator.index,
                next: comp.next
              }
            }
          }
          break;
        case 'photo':
          if (message.photo) {
            return {
              res: message.photo[message.photo.length-1].file_id,
              index: comparator.index,
              next: comp.next
            }
          }
          break;
        case 'video':
          if (message.video) {
            return {
              res: message.video.file_id,
              index: comparator.index,
              next: comp.next
            }
          }
          break;
        case 'voice':
          if (message.voice) {
            return {
              res: message.voice.file_id,
              index: comparator.index,
              next: comp.next
            }
          }
        case 'audio':
          if (message.audio) {
            return {
              res: message.audio.file_id,
              index: comparator.index,
              next: comp.next
            }
          }
          break;
        case 'location':
          if (message.location) {
            return {
              res: message.location,
              index: comparator.index,
              next: comp.next
            }
          }
          break;
        case 'contact':
          if (message.contact) {
            return {
              res: message.contact,
              index: comparator.index,
              next: comp.next
            }
          }
          break;
        case 'document':
          if (message.document) {
            if (comp.mime) {
              if (comp.mime.toLowerCase() == message.document.mime_type.toLowerCase()) return {
                  res: message.document,
                  index: comparator.index,
                  next: comp.next
              }
            } else {
              return {
                res: message.document,
                index: comparator.index,
                next: comp.next
              }
            }
          }
          break;
        case 'sticker':
          if (message.sticker) {
            return {
              res: message.sticker,
              index: comparator.index,
              next: comp.next
            }
          }
        case 'inline':
          if (message.data) {
            if (comp.reg) {
              if (comp.reg.test(message.data)) {
                return {
                  res: message.data,
                  index: comparator.index,
                  next: comp.next
                }
              }
            } else {
              return {
                res: message.data,
                index: comparator.index,
                next: comp.next
              }
            }
          }

          break;
        default:
          return false;
      }
    }
  }
  function inAction(id) {
    var t = false;
    sessions.forEach(function(s) {
      if (s.id == id) {
        t = true;
      }
    })
    return t;
  }
  function mrStopper(trigger, id) {
    var d = -1;
    trigger.stopOn.force.forEach(function(s, i) {
      if (s.uid == id) {
        if (s.stop) {
          s.stop = false;
          delById(s.uid);
          d = i;
          stop_callback(id, true)
        }
      }
    })
    trigger.stopOn.force.splice(d, 1)
  }
  function setArr(arr, elt, what) {
    if (arr.length - 1 < elt) {
      for (var i = arr.length; i <= elt; i++) {
        if (elt == i) {
          arr.push(what);
        } else {
          arr.push('');
        }
      }
    } else {
      arr[elt] = what;
    }
    return arr;
  }
  function setLastChat(uid, cid) {
    sessions.forEach(function (s) {
      if(s.id == uid) {
        s.lastChat = cid;
      }
    })
  }
  bot.on('callback_query', function(message) {
    var uid = message.from.id;
    var msg = message.data;
    var st = getStage(uid);
    var tt = compareByType(message, trigger.startOn)
    var ss = compareByType(message, trigger.stopOn)

    var valid = false;
    if (trigger.stopOn.reg && tt) {
      valid = true;
    } else {
      if (!trigger.stopOn.reg && trigger.stopOn.data == msg) {
        valid = true;
      }
    }

    if (valid && ss && inAction(uid)) {
      stop_callback(uid, false, getAnswers(uid));
      delById(uid);
    }
    valid = false;
    if (trigger.startOn.reg && tt) {
      valid = true;
    } else {
      if (!trigger.startOn.reg && trigger.startOn.data == msg) {
        valid = true;
      }
    }
    if (valid && !inAction(uid)) {

      start_callback(uid, message.from.firstname, message, (ok, n) => {
        if (ok) {
          if (n) {
            nextStage(uid, n);
          } else {
            nextStage(uid, tt.next)
          }
          sendByType(uid, questions, 0)
        }
      })
    } else {
      mrStopper(trigger, uid)
      sessions.forEach(function(s) {
        if (s.id == uid) {
          var ct = compareByType(message, questions[st])
          if (ct) {
            s.answers = setArr(s.answers, ct.index, ct.res)
            if (st == -1) {
            } else {
              var flg = ct.next ? ct.next : st + 1;
              if (flg > questions.length - 1) {
                end_callback(uid, getAnswers(uid), msg, (n)=>{
                  if(!n){
                    delById(uid);
                  }else{
                    nextStage(uid, n);
                  }
                })
                // delById(uid);
              } else {
                mid_callback(uid, getAnswers(uid), st, (ok, n) => {
                  if (ok) {
                    if (n) {
                      nextStage(uid, n);
                    } else {
                      nextStage(uid, ct.next)
                    }
                    // setLastChat(uid,message.chat.id)
                    sendByType(uid, questions, getStage(uid))
                  } else {
                    if (n) {
                      nextStage(uid, n);
                      sendByType(uid, questions, getStage(uid));
                    } else {
                      end_callback(uid, getAnswers(uid), msg)
                      delById(uid);
                    }

                  }
                })

              }
            }
          } else {
            if (questions[st].err) {
              bot.sendMessage(uid, questions[st].err)
            }
          }
        }
      })
    }
  })

  bot.on('message', function(message) {
    var uid = message.from.id;
    var msg = message.text;
    var st = getStage(uid);
    var tt = compareByType(message, trigger.startOn)
    var ss = compareByType(message, trigger.stopOn)
    var va = compareByData(message, trigger.startOn)
    var sa = compareByData(message, trigger.stopOn)
    if (sa && ss && inAction(uid)) {
      stop_callback(uid, false, getAnswers(uid));
      delById(uid);
    }
    if (va && tt && !inAction(uid)) {
      start_callback(uid, message.from.firstname, message, (ok, n) => {
        if (ok) {
          if (n) {
            nextStage(uid, n);
          } else {
            nextStage(uid, tt.next);
          }
          sendByType(uid, questions, 0)
        } else {
          stop_callback(uid, false, getAnswers(uid));
          delById(uid);
        }
      })
    } else {
      mrStopper(trigger, uid)
      sessions.forEach(function(s) {
        if (s.id == uid) {
          var ct = compareByType(message, questions[st])
          if (ct) {
            s.answers = setArr(s.answers, ct.index, ct.res)
            if (st == -1) {
            } else {
              var flg = ct.next ? ct.next : st + 1;
              if (flg > questions.length - 1) {
                end_callback(uid, getAnswers(uid), (n)=>{
                  if(!n){
                    delById(uid);
                  }else{
                    nextStage(uid, n);
                  }
                })
              } else {
                mid_callback(uid, getAnswers(uid), st, (ok, n) => {
                  if (ok) {
                    if (n) {
                      nextStage(uid, n)
                    } else {
                      nextStage(uid, ct.next)
                    }
                    sendByType(uid, questions, getStage(uid))
                  } else {
                    if (n) {
                      nextStage(uid, n);
                      sendByType(uid, questions, getStage(uid));
                    } else {
                      stop_callback(uid, false, getAnswers(uid));
                      delById(uid);
                    }

                  }
                })

              }
            }
          } else {
            if (questions[st].err) {
              bot.sendMessage(uid, questions[st].err)
            }
          }
        }
      })
    }

  })

}
/// arg1 - Названия кнопок arg2 - callback_data кнопок, rows - количество колонок inline - инлайн или нет
function makeButtons(arg1, arg2, rows, inline) {
  var eee = []
  var eee2 = []
  var ff = 0;
  var r = rows ? rows : 1;
  var l = 0;
  for (var i = 0; i < arg1.length; i++) {
    eee2.push({
      text: arg1[i],
      callback_data: arg2[i]
    })
    l++
    ff++
    if (l == r) {
      eee.push(eee2)
      l = 0;
      eee2 = [];
    }
  }
  var s = eee.length;
  var mod = (arg1.length * 1) % (rows * 1);
  if (mod % 2 == 0) {
    eee2 = [];
    l = 0
    for (var i = 0; i < mod; i++) {
      eee2.push({
        text: arg1[s * (r * 1) + i],
        callback_data: arg2[s * (r * 1) + i]
      })
      l++
      ff++
      if (l == 2) {
        eee.push(eee2)
        l = 0;
        eee2 = [];
      }
    }
  } else {
    if ((arg1.length * 1) > (rows * 1) && mod != 0) {
      for (var i = 0; i < mod; i++) {
        eee.push([{
          text: arg1[s * (r * 1) + i],
          callback_data: arg2[s * (r * 1) + i]
        }])
      }
    }
  }


  if (inline) {
    var asd = {
      reply_markup: JSON.stringify({
        inline_keyboard: eee,
        resize_keyboard: true
      })
    }
  } else {
    var asd = {
      reply_markup: JSON.stringify({
        keyboard: eee,
        resize_keyboard: true
      })
    }
  }
  return asd;
}

//// wait for answer function  /// "bot" - Bot, initButtons - Кнопки которые нужно выводить (массив кнопок)
//// inline - Инлайновые? "Buttext" - Текст при выводе этих кнопок. "command" Команда при которой будут выводиться эти кнопки!
//// callback - Колбэк ! 1 аргумент - id чела который попался, 2 Аргумент - callback_data или Индекс  нажатой кнопки
function waitFor(bot, initButtons, inline, Buttext, command, callback) { // Сделать колюбэк с индексом кнопки и ответом

  bot.on('message', function(message) {
    var uid = message.from.id;
    var msg = message.text;
    if (inline) {
      var options = {
        reply_markup: JSON.stringify({
          inline_keyboard: initButtons
        })
      };
    } else {
      var options = {
        reply_markup: JSON.stringify({
          keyboard: initButtons
        })
      };
    }
    if (msg == command.toLowerCase()) {
      bot.sendMessage(uid, Buttext, options)
    }
  })

  if (inline) {
    bot.on('callback_query', function(msg) {
      var uid = msg.from.id;
      var data = msg.data;
      // callback here
      initButtons.forEach(function(ib, i) {
        if (ib[0].callback_data == data) {
          callback(uid, data);
        }
      })

    // callback here
    })
  } else {
    bot.on('message', function(msg) {
      var uid = msg.from.id;
      var data = msg.text;
      // callback here
      initButtons.forEach(function(ib, i) {
        if (ib[0].text == data) {
          callback(uid, i);
        }
      })

    // callback here
    })
  }

}
// awesome function

module.exports.askFor = askFor;
module.exports.waitFor = waitFor;
module.exports.makeButtons = makeButtons;
