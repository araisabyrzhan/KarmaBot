// var BotHelper = require('./BotFunctions');
var fs = require('fs');
var MyNewBot = require('node-telegram-bot-api');
var token = '448539824:AAEr9d17VoXVPg02Suytf_v0ItKcIYkKsOg';
// 392846540
var bot = new MyNewBot(token, {
  polling: true
});
var Users = require('./models/users');
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/karma');



bot.on('callback_query', function onCallbackQuery(callbackQuery) {
  const action = callbackQuery.data;
  const msg = callbackQuery.message;
  const opts = {
    chat_id: msg.chat.id,
    message_id: msg.message_id,
  };
  let text;

  if (action === '1') {
    text = 'You hit button 1';
  }

  bot.editMessageText(text, opts);
});


bot.on('chat_message', function (msg) {
	//console.log(msg);
	bot.getChatMembersCount(msg.chat.id).then(function(w) {
    //console.log(w)
  })
	bot.sendMessage(msg.chat.id, 'www')
  // принимает любые сообщения которые приходят боту, кроме inline_buttons
})
bot.onText(/^\/start/i, function (msg) {  // принимает текстовое сообщение которое соответствует reg Expression
 //console.log(msg)   // msg содержит информацию о сообщении 
  if(msg.from.username!= undefined){
  Users.findOne({id:msg.from.id},function(err,u) {
    if(u){
      bot.sendMessage(u.id, 'Вы уже зарегистрированы')
    } else {
      var user = new Users({
      id : msg.from.id,
      firstname : msg.from.first_name,
      lastname : msg.from.last_name,
      username : msg.from.username
    })
    user.save();
    bot.sendMessage(msg.from.id, 'Спасибо, '+ msg.from.first_name +'! Ваши данные успешно сохранены.' )
    }
  })
  }
  else{
  	bot.sendMessage(msg.from.id, 'Пожалуйста создайте свой username, затем еще раз нажмите /start.' ) // отправляет сообщение (id, message)
  }
})
// 🥇🥇🥇🥇🎖🏅 

bot.onText(/^\/karma\s(.+)/i, function (msg,match) {  // принимает текстовое сообщение которое соответствует /karma @username +/-
  // \s([^\>]*)
  var result = match[1].split(' ')
    //console.log('accepted',match[1],result)

  if(result.length < 2){
    if(result[0][0]=="@"){
       result[0] = result[0].match(/\@(.+)/)[1]
       ///karma @username 
       Users.findOne({username:result[0]},function(err,u) {
        if(u && result[0]==msg.from.username){
            bot.sendMessage(msg.from.id, 'Ваш результат: ' + u.karma)
        }else{
          bot.sendMessage(msg.chat.id, 'Вы не можете рассматривать чужие результаты')
        }
      })
    } 
  }else{

    result[0] = result[0].match(/\@(.+)/)[1]
    //console.log(msg,result)   // msg содержит информацию о сообщении
    if( result[1].length== 1 && result[1].match(/[\+|\-]/)){
      //usernames = [result[0], msg.from.username];
      var karmasender = msg.from.username
      var karmagetter = result[0]
      //Users.find({"$and": [{"username": karmagetter}, {"username": karmasender}]},function(errg, getuser) {
      Users.findOne({"username": karmagetter}, function(err1, getter) {

         Users.findOne({"username": karmasender}, function(err2, sender) {

          if(getter && sender){          
            //console.log('Getter: ', getter , ' Sender: ', sender)
            var pass = false;
            var today = new Date().getMonth()+'-'+new Date().getDate();
            
            if(sender.senddate){
              var comp = new Date(sender.senddate).getMonth()+'-'+new Date(sender.senddate).getDate()
              //console.log('***today: '+ today + ' senddate:'+ comp + ' ////')
              if(comp==today){
                pass=false; 
              }else{
                pass=true;
              }
            }else{
              pass = true;
            }

            /* Проверка отправки кармы на себя*/
            
              if(getter.username == msg.from.username){
                bot.sendMessage(msg.from.id, 'Ваш результат: ' + getter.karma)
                bot.sendMessage(msg.chat.id, 'Вы не можете изменить количество своих карм')
              }
              else{
                 if(pass) {
                    if(result[1].trim()=='+'){
                      getter.karma++;
                      bot.sendMessage(msg.chat.id, 'К ' + getter.firstname +' добавлена +1 карма')
                      bot.sendMessage(getter.id, 'Вам добавлена +1 карма')
                    } else if(result[1].trim()=='-'){
                      getter.karma-=2;
                      bot.sendMessage(msg.chat.id, 'У ' + getter.firstname +' было отнято -2 кармы')
                      bot.sendMessage(getter.id, 'У Вас отнято -2 кармы')
                    }
                    sender.senddate=new Date();
                    sender.save();
                    getter.save();
                  }else{
                    bot.sendMessage(msg.chat.id, 'За сегодня Ваш лимит кармы истек')
                  }
              }

          }else if(err1){
            
            bot.sendMessage(msg.chat.id,  'Пользователь не зарегистрирован')
          
          }else{
            
            bot.sendMessage(msg.chat.id,  ' Ошибка ')
          
          }
        })
        
      })

    }else{
    	bot.sendMessage(msg.chat.id, 'Ошибка в "' + result[1] + '". Правильный формат: "/karma @autousername +" или "/karma @autousername -"' )
    }
	}
  
})

bot.onText(/^\/karmastats/i, function (msg) {  // принимает текстовое сообщение который выводит ТОП 5 пользователей с медалями
 Users.find().sort('-karma').limit(5).exec(function(e,u) {
          var top = [];
          u.map(function(user, index){
            top.push('\n' +  ((index==0)? '🥇': (index ==1)? '🥈': (index ==2)? '🥉': (index ==3)? '🎖':'🏅') + ' ' + user.firstname + ' (+'+ user.karma +')' )     
          })/* 🥇🥈🥉🎖🏅*/
          bot.sendMessage(msg.chat.id, 'TOP 5: ' + top)       
        })
})

bot.onText(/^\/karmame/i, function (msg) {  // принимает текстовое сообщение который выводит свой результат
 Users.findOne({username:msg.from.username},function(err,u) { if(u){ bot.sendMessage(msg.from.id, 'Ваш результат: ' + u.karma)}})
})

bot.onText(/^\/karmafull/i, function (msg) {  // принимает текстовое сообщение который должен вывести весь список пользователей карм

  if(msg.from.username == 'rus_as'){
    Users.find().sort('-karma').exec(function(e,u) {
          var top = [];
          u.map(function(user, index){
            top.push('\n' +  (index + 1) + ' ' + user.firstname + ' (+'+ user.karma +')' )     
          })
          bot.sendMessage(msg.chat.id, 'Весь список: ' + top)       
    })
  }else{
    bot.sendMessage(msg.chat.id, 'У Вас ограничен доступ')
  }
        
})
//Users.find({},function(e,u) {console.log(u)})

module.exports = bot
