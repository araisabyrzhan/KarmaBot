var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var Schema = mongoose.Schema; 
var karma = new Schema({
	id : {type:String, required: true, unique: true} ,
	firstname : {type:String} ,
	lastname : {type:String} ,
	username : {type:String, required: true, unique: true} ,
	karma :  {type: Number, default: 0} ,
	senddate : {type : Date}
});

module.exports = mongoose.model('karma', karma);